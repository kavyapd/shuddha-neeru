package com.sanradiance.mobilewpp;

public class ConstantValues {
    public final String CONSTANT_LOW = "LOW";
    public final String CONSTANT_HALF = "HALF";
    public final String CONSTANT_FULL = "FULL";
    public final String CONSTANT_NOPOWER = "no_power";
    public final String CONSTANT_NOWATER = "no_water";
    public final String CONSTANT_BREAKDOWN = "break_down";
    public final String CONSTANT_NOTAPP = "not_applicable";

}
